﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus12_13
{
    class AnkkalinnanAsukki
    {
        private string Etunimi;
        private string Sukunimi;

        public AnkkalinnanAsukki(string etunimi, string sukunimi)
        {
            Etunimi = etunimi;
            Sukunimi = sukunimi;
        }

        public string annaEtunimi() { return Etunimi; }
        public string annaSukunimi() { return Sukunimi; }
        public string annaNimi() { return Etunimi + " " + Sukunimi; }

    }
}
