﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus12_13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Osa1 suorittaminen
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Suoritetaan ohjelmaa 'Osa 1'");
            Console.ResetColor();

            Osa1();
            Console.Clear();

            //Osa2 suorittaminen
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Suoritetaan ohjelmaa 'Osa 2'");
            Console.ResetColor();

            Osa2();
            Console.Clear();

            //Osa3 súorittaminen
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Suoritetaan ohjelmaa 'Osa 3'");
            Console.ResetColor();

            Osa3();
            Console.Clear();


            Console.WriteLine("ohjelma päättyy...");
            Console.ReadLine();
        }
        
        //tulosta listan sisältö (int)
        public static void tulostaLista(List<int> lista)
        {
            Console.Write("[ ");

            for(int i = 0; i < lista.Count; i++)
            {
                if (i == (lista.Count - 1))
                {
                    Console.Write(lista[i]);
                }
                else
                {
                    Console.Write(lista[i] + ", ");
                }
            }

            Console.Write(" ] \n");
        }

        //tulosta listan sisältö (string)
        public static void tulostaLista(List<string> lista)
        {
            Console.Write("[ ");

            for (int i = 0; i < lista.Count; i++)
            {
                if (i == (lista.Count - 1))
                {
                    Console.Write(lista[i]);
                }
                else
                {
                    Console.Write(lista[i] + ", ");
                }
            }

            Console.Write(" ] \n");
        }

        //tulosta listan sisältö (Ankkalinnan asukit)
        public static void tulostaLista(List<AnkkalinnanAsukki> lista)
        {
            Console.Write("[ ");

            for (int i = 0; i < lista.Count; i++)
            {
                if (i == (lista.Count - 1))
                {
                    Console.Write(lista[i].annaNimi());
                }
                else
                {
                    Console.Write(lista[i].annaNimi() + ", ");
                }
            }
            Console.Write(" ] \n");
        }
        

        //AnkkalinnanAsukit luokkien vertaaminen nimen perusteella
        public static int vertaaAsukkaat(AnkkalinnanAsukki a, AnkkalinnanAsukki b)
        {
            return string.Compare(a.annaEtunimi(), b.annaEtunimi());
        }

        public static void Osa1()
        {

            
            List<int> lista = new List<int> {
                12, 
                36,
                45,
                111,
                869,
                123,
                1,
                13
            };

            Console.WriteLine("Aluksi meillä on olemassa lista:");
            tulostaLista(lista);
            
            //lisätään luku 26
            Console.WriteLine("lisätään arvo '26' listaan");
            lista.Add(26);

            Console.WriteLine("Luodaan testi lista. Testi listan sisältö:");
            
            //testilista
            List<int> testList = new List<int>();
            testList.Add(2);
            testList.Add(3);
            testList.Add(4);
            tulostaLista(lista);

            //lisätään luvut 
            Console.WriteLine("lisätään testilistan arvot aluksi luotuun listaan");
            lista.AddRange(testList);
            tulostaLista(lista);

            Console.WriteLine($"Listassa tällä hetkellä {lista.Capacity} elementtiä");

            Console.WriteLine($"Tyhjennetään testi lista");
            testList.Clear();
            tulostaLista(testList);

            //sisältääkö lista arvon 2?
            Console.WriteLine($"Onko alkuperäisessä listassa luku 2?");
            bool query = lista.Contains(2);

            if(query == true)
            {
                Console.WriteLine($"Kyllä");
            } else
            {
                Console.WriteLine($"Ei");
            }

            //Kuinka monta elementtia listassa on
            int count = lista.Count;
            Console.WriteLine($"Listassa tällä hetkellä {lista.Count} elementtiä");

            //Millä sijalla luku 10 on listassa
            Console.WriteLine("Etsitään arvoa 10 listasta");
            int whereIs = lista.IndexOf(10);

            if (whereIs == -1)
            {
                Console.WriteLine("Ei löydetty listasta");
            } else
            {
                Console.WriteLine("Arvo 10 on listan sijalla: " + whereIs);
            }

            //lisätään arvo 777 listan sijalle 
            Console.WriteLine($"Lisätään listan loppuun arvot 777, 145, 106");
            lista.Insert(0, 777);
            lista.Insert(1, 145);
            lista.Insert(2, 106);

            Console.WriteLine($"Lisätään listan alkuun arvo 20, 84, 46, 21, 33");
            lista.Insert(0, 20);
            lista.Insert(0, 84);
            lista.Insert(0, 46);
            lista.Insert(0, 21);
            lista.Insert(0, 33);

            tulostaLista(lista);

            //poistaa ensimmäisen arvon 777 listasta
            Console.WriteLine($"Listasta poistetaan arvo 777");
            lista.Remove(777);
            tulostaLista(lista);

            //poista luku listan sijalla 0
            Console.WriteLine($"Listasta poistetaan arvo, joka on sijalla 0");
            lista.RemoveAt(0);
            tulostaLista(lista);

            //poista alusta 2 ensimmäistä lukua
            Console.WriteLine($"Listasta poistetaan 2 ensimmäistä arvoa");
            lista.RemoveRange(0, 2);
            tulostaLista(lista);

            //lajittele lista
            Console.WriteLine($"Lajitellaan listan arvot");
            lista.Sort();
            tulostaLista(lista);

            Console.WriteLine("Listan kapasiteetti ennen leikkaamista (TrimExcess): " + lista.Capacity);
            Console.WriteLine($"Leikataan listaa");
            lista.TrimExcess();
            Console.WriteLine("Listan kapasiteetti leikkaamisen jälkeen " + lista.Capacity);
            tulostaLista(lista);

            
            Console.WriteLine($"Osa 1 päättyy...");
            Console.SetCursorPosition(0, 0);
            Console.ReadLine();
        }

        public static void Osa2()
        {
            List<string> lista = new List<string> {
                "Sana1",
                "Sana2",
                "Sana3",
                "Sana4"
            };

            //lisätään luku 'testiA'
            Console.WriteLine("lisätään arvo 'testiA' listaan");
            lista.Add("testiA");


            Console.WriteLine("Luodaan testi lista");

            //testilista
            List<string> testList = new List<string>();
            testList.Add("testiB");
            testList.Add("testiC");
            testList.Add("testiD");
            tulostaLista(lista);

            //lisätään luvut 
            Console.WriteLine("lisätään testilistan arvot aluksi luotuun listaan");
            lista.AddRange(testList);
            tulostaLista(lista);

            Console.WriteLine($"Listassa tällä hetkellä {lista.Capacity} elementtiä");

            Console.WriteLine($"Tyhjennetään lista");
            lista.Clear();
            tulostaLista(lista);

            //sisältääkö lista arvon 2?
            Console.WriteLine($"Onko listassa luku testiC?");
            bool query = lista.Contains("testiC");

            if (query == true)
            {
                Console.WriteLine($"Kyllä");
            }
            else
            {
                Console.WriteLine($"Ei");
            }

            //Kuinka monta elementtia listassa on
            int count = lista.Count;
            Console.WriteLine($"Listassa tällä hetkellä {lista.Count} elementtiä");

            //Millä sijalla luku 10 on listassa
            Console.WriteLine("Etsitään arvoa 'testiB' listasta");
            int whereIs = lista.IndexOf("testiB");

            if (whereIs == -1)
            {
                Console.WriteLine("Ei löydetty listasta");
            }
            else
            {
                Console.WriteLine(whereIs);
            }

            //lisätään arvo 777 listan sijalle 
            Console.WriteLine($"Lisätään listan loppuun arvot test1, test2, test3");
            lista.Insert(0, "test1");
            lista.Insert(1, "test2");
            lista.Insert(2, "test3");

            Console.WriteLine($"Lisätään listan alkuun arvo test10, test11, test12, test13, test14");
            lista.Insert(0, "test10");
            lista.Insert(0, "test11");
            lista.Insert(0, "test12");
            lista.Insert(0, "test13");
            lista.Insert(0, "test14");

            tulostaLista(lista);

            //poistaa ensimmäisen arvon 777 listasta
            Console.WriteLine($"Listasta poistetaan arvo 'test10'");
            lista.Remove("test10");
            tulostaLista(lista);

            //poista luku listan sijalla 0
            Console.WriteLine($"Listasta poistetaan arvo, joka on sijalla 0");
            lista.RemoveAt(0);
            tulostaLista(lista);

            //poista alusta 2 ensimmäistä lukua
            Console.WriteLine($"Listasta poistetaan 2 ensimmäistä arvoa");
            lista.RemoveRange(0, 2);
            tulostaLista(lista);

            //lajittele lista
            Console.WriteLine($"Lajitellaan listan arvot");
            lista.Sort();
            tulostaLista(lista);

            Console.WriteLine("Listan kapasiteetti ennen leikkaamista (TrimExcess): " + lista.Capacity);
            Console.WriteLine($"Leikataan listaa");
            lista.TrimExcess();
            Console.WriteLine("Listan kapasiteetti leikkaamisen jälkeen " + lista.Capacity);
            tulostaLista(lista);

            Console.WriteLine($"Osa 2 päättyy...");
            Console.SetCursorPosition(0, 0);
            Console.ReadLine();
        }

        public static void Osa3()
        {
            var ankkalinnanAsukit = new List<AnkkalinnanAsukki>
            {
                new AnkkalinnanAsukki("Tupu", "Ankka"),
                new AnkkalinnanAsukki("Hupu", "Ankka"),
                new AnkkalinnanAsukki("Lupu", "Ankka"),
                new AnkkalinnanAsukki("Mikki", "Hiiri")
            };

            //lisätään luku 'testiA'
            Console.WriteLine("lisätään arvo 'testiA' listaan");
            ankkalinnanAsukit.Add(new AnkkalinnanAsukki("Aku", "Ankka"));


            Console.WriteLine("Luodaan testi lista");

            //testilista
            List<AnkkalinnanAsukki> testList = new List<AnkkalinnanAsukki>
            {
                new AnkkalinnanAsukki("Joku","Nimi"),
                new AnkkalinnanAsukki("Toinen","Nimi"),
                new AnkkalinnanAsukki("Kolmas","Nimi")
            };
            tulostaLista(ankkalinnanAsukit);

            //lisätään luodut asukkaat 
            Console.WriteLine("lisätään testilistan arvot aluksi luotuun listaan");
            ankkalinnanAsukit.AddRange(testList);
            tulostaLista(ankkalinnanAsukit);

            Console.WriteLine($"Listassa tällä hetkellä {ankkalinnanAsukit.Capacity} elementtiä");
            
            //sisältääkö ankkalinnanAsukit asukkaan nimellä 'Hessu Hopo'"?
            Console.WriteLine($"Onko listassa arvo 'Aku'?");

            bool query = ankkalinnanAsukit.Contains(new AnkkalinnanAsukki("Hessu", "Hopo"));

            if (query == true)
            {
                Console.WriteLine($"Kyllä");
            }
            else
            {
                Console.WriteLine($"Ei");
            }

            //Kuinka monta elementtia listassa on
            int count = ankkalinnanAsukit.Count;
            Console.WriteLine($"Listassa tällä hetkellä {ankkalinnanAsukit.Count} elementtiä");

            //Millä sijalla luku 10 on listassa
            Console.WriteLine("Etsitään arvoa 'Hupu Ankka' listasta");
            int whereIs = ankkalinnanAsukit.IndexOf(new AnkkalinnanAsukki("Hupu", "Ankka"));

            if (whereIs == -1)
            {
                Console.WriteLine("Ei löydetty listasta");
            }
            else
            {
                Console.WriteLine(whereIs);
            }

            Console.WriteLine($"Tyhjennetään ankkalinnanAsukit");
            ankkalinnanAsukit.Clear();
            tulostaLista(ankkalinnanAsukit);

            
            Console.WriteLine($"Lisätään listan alkuun arvo test10, 'test, name', 'test2, name', 'test3, name'");
            ankkalinnanAsukit.Insert(0, new AnkkalinnanAsukki("test", "name"));
            ankkalinnanAsukit.Insert(0, new AnkkalinnanAsukki("test2", "name"));
            ankkalinnanAsukit.Insert(0, new AnkkalinnanAsukki("test3", "name"));

            tulostaLista(ankkalinnanAsukit);

            //lisätään arvo 777 listan sijalle 
            Console.WriteLine($"Lisätään listan loppuun arvot test1_2, test1_3");
            ankkalinnanAsukit.Insert(ankkalinnanAsukit.Count, new AnkkalinnanAsukki("test1_2", "name_2"));
            ankkalinnanAsukit.Insert(ankkalinnanAsukit.Count, new AnkkalinnanAsukki("Asukas", "name_2"));
            tulostaLista(ankkalinnanAsukit);

            //poistaa ensimmäisen arvon 777 listasta
            Console.WriteLine($"Listasta poistetaan arvo 'test1_2'");
            ankkalinnanAsukit.Remove(new AnkkalinnanAsukki("test1_2", "name_2"));
            tulostaLista(ankkalinnanAsukit);

            //poista luku listan sijalla 0
            Console.WriteLine($"Listasta poistetaan arvo, joka on sijalla 0");
            ankkalinnanAsukit.RemoveAt(0);
            tulostaLista(ankkalinnanAsukit);

            //poista alusta 2 ensimmäistä lukua
            Console.WriteLine($"Listasta poistetaan 2 ensimmäistä arvoa");
            ankkalinnanAsukit.RemoveRange(0, 2);
            tulostaLista(ankkalinnanAsukit);

            //lajittele ankkalinnanAsukit
            Console.WriteLine($"Lajitellaan listan arvot");
            
            ankkalinnanAsukit.Sort(vertaaAsukkaat);
            tulostaLista(ankkalinnanAsukit);

            Console.WriteLine("Listan kapasiteetti ennen leikkaamista (TrimExcess): " + ankkalinnanAsukit.Capacity);
            Console.WriteLine($"Leikataan listaa");
            ankkalinnanAsukit.TrimExcess();
            Console.WriteLine("Listan kapasiteetti leikkaamisen jälkeen " + ankkalinnanAsukit.Capacity);
            tulostaLista(ankkalinnanAsukit);

            Console.WriteLine($"Osa 3 päättyy...");
            Console.SetCursorPosition(0, 0);
            Console.ReadLine();

        }
        
        
    }
}
