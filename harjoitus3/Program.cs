﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus3
{
    class Program
    {
        public static List<Tili> tilit = new List<Tili>();

        static void Main(string[] args)
        {
            Tili JaneGreen = new Tili("Jane Green", 50);
            Tili JohnBlue = new Tili("John Blue", 0);

            tilit.Add(JaneGreen);
            tilit.Add(JohnBlue);

            Console.WriteLine($"{JaneGreen.Nimi}:n saldo: {JaneGreen.Saldo}");
            Console.WriteLine($"{JohnBlue.Nimi}:n saldo: {JohnBlue.Saldo}\n");

            Console.Write("Anna talletuksen määrä tilille Jane Green: ");
            decimal talletus = Convert.ToDecimal(Console.ReadLine());

            JaneGreen.Talletus(talletus);

            Console.WriteLine($"{JaneGreen.Nimi}:n saldo: {JaneGreen.Saldo}");
            Console.WriteLine($"{JohnBlue.Nimi}:n saldo: {JohnBlue.Saldo}\n");

            Console.Write("Anna talletuksen määrä tilille John Blue: ");
            decimal talletus2 = Convert.ToDecimal(Console.ReadLine());

            JohnBlue.Talletus(talletus2);

            Console.WriteLine($"{JaneGreen.Nimi}:n saldo: {JaneGreen.Saldo}");
            Console.WriteLine($"{JohnBlue.Nimi}:n saldo: {JohnBlue.Saldo}\n");
            
            Console.WriteLine("Ohjelma päättyy");
            Console.ReadLine();
        }
        

        public static void lisaaRahaaValikko()
        {
            bool jatkaviela = true;
            while (jatkaviela) { 
                Console.WriteLine("Valitse tili jolle lisätään rahaa:");

                try
                {

                    for (int x = 0; x < (tilit.Count); x++)
                    {
                        Console.WriteLine($"[{x}] {tilit[x].Nimi}");
                    }

                    Console.WriteLine($"[{tilit.Count}] Exit");

            
                    int valinta2 = Convert.ToInt16(Console.ReadLine());

                    if (valinta2 == (tilit.Count))
                    {
                        Console.WriteLine("Palataan valikkoon");
                        jatkaviela = false;
                        Console.Clear();
                    } else if (valinta2 < 0 || valinta2 > (tilit.Count))
                    {
                        Console.WriteLine("Tuntematon valinta");
                    }
                    else
                    {
                        laitaRahaaTilille(tilit[valinta2]);
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Tuntematon valinta");
                }
            }
        }

        public static void laitaRahaaTilille(Tili tili)
        {
            Console.WriteLine($"Maaritta kuinka paljon laitetaan tilille {tili.Nimi}. Tililä on jo saldoa {tili.Saldo}");
  
            try
            {
                decimal summa = Convert.ToDecimal(Console.ReadLine());

                if (summa <= 0)
                {
                    Console.WriteLine("Laitettavan saldon on oltava suurempi kuin 0");
                } else
                {
                    tili.Talletus(summa);
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Tuntematon valinta");
            }
            
        }

        public static void paaLooppi()
        {
            bool jatka = true;

            while (jatka)
            {
                foreach(Tili tili in tilit)
                {
                    Console.WriteLine($"{tili.Nimi} tilin saldo: {tili.Saldo}");
                }
                
                Console.WriteLine("[1] Lisää rahaa tilille\n" +
                    "[2] Poistu ohjelmasta");

                try { 
                    int valinta = Convert.ToInt16(Console.ReadLine());
                    switch (valinta)
                    {
                        case 1:
                            lisaaRahaaValikko();
                            break;
                        case 2:
                            jatka = false;
                            Console.Clear();
                            break;
                        default:
                            Console.WriteLine("Valinta ei ole määritelty");
                            break;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Valinta ei ole määritelty");
                }
            }

        }

    }
}
