﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus3
{
    class Tili
    {
        public string Nimi;

        public decimal Saldo;

       
        public Tili(string nimi, decimal alkuperaissaldo)
        {
            Nimi = nimi;
            Saldo = alkuperaissaldo;
        }

        public void Talletus(decimal talletusMaara)
        {
            if(talletusMaara < 0)
            {
                Console.WriteLine("Talletus ei voi olla negatiivinen");
            }
            else
            {
                Saldo += talletusMaara;
                Console.WriteLine($"Lisätään {talletusMaara} euroa tilille {Nimi}.");
            }
        }
    }
}
