﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus6
{
    class Program
    {

        public static int virheet;
        public static string salasana;

        static void Main(string[] args)
        {
            
            virheet = 0;
            salasana = "K1967";
            bool jatka = true;

            while (jatka) { 
                Console.ForegroundColor = ConsoleColor.Yellow;

                int yritykset = (3 - virheet);

                Console.WriteLine($"Testi salasana = {salasana}, sinulla on {yritykset} yritystä kunnes ohjelma sulkee itsensä.");
                Console.ResetColor();
            
                Console.WriteLine("Anna ensimmäinen koodi: (3 kirjainta)");
                string syote1 = Console.ReadLine();

                Console.WriteLine("Anna seuraava koodi: (2 kirjainta)");
                string syote2 = Console.ReadLine();
            
                if(syotteenHallinta(syote1, syote2))
                {
                    //hyväksyttävät syötteet
                    Console.WriteLine($"Tervetuloa ohjelmaan. Syötteiden antamiseen meni {virheet} yritystä");
                    jatka = false;
                }
                else
                {
                    //syötteet eivät olleet hyväksyttäviä
                    if(virheet > 3)
                    {
                        Console.WriteLine("Liian monta yritystä. Ohjelma päätetään...");
                        jatka = false;
                    }
                }
            }
            Console.WriteLine("Ohjelma päätetään...");
            Console.ReadLine();
        }

        public static bool syotteenHallinta(string osa1, string osa2)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            string firstPart = "", lastPart = "";
            int virheita = 0;

            if (osa1.Length < 3)
            {
                Console.WriteLine("Ensimmäinen syote on liian lyhyt. Syötteen oltava 3 merkkiä pitkä");
                virheita++;
            } else
            {
                firstPart = salasana.Substring(0, 3);
                if(firstPart != osa1)
                {
                    Console.WriteLine("Ensimmäinen syote ei vastaa kysyttyä tunnusta");
                    virheita++;
                }
            }

            if (osa2.Length == 0)
            {
                Console.WriteLine("Toinen syöte ei voi olla tyhjä");
                virheita++;
            }
            else
            {
                lastPart = salasana.Substring(3);
                if (lastPart != osa2)
                {
                    Console.WriteLine("Toinen syote ei vastaa kysyttyä tunnusta");
                    virheita++;
                }
            }

            Console.ResetColor();

            if(virheita > 0)
            {
                virheet++;
                return false;
            } else
            {
                return true;
            }

        }

    }
}
