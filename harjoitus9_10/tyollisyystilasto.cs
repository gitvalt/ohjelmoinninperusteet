﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus9_10
{
    class Tyollisyystilasto
    {


        private DateTime tmp_Vuosi;
        private string Vuosi
        {
            get
            {
                return tmp_Vuosi.Year.ToString();
            }
            set
            {
                try {    
                    tmp_Vuosi = DateTime.Parse("1-1-" + value);
                    
                }
                catch (FormatException)
                {
                    throw new FormatException("Vuosi ei ollut hyväksyttävä arvo");
                }
            }
        }
        private int Neljannes;

        public enum osaAlueet { Osa_aikaiset, Määräaikaisia, Vaikeasti_täytettäviä };

        //{ { {osa-alue, lukumäärä}, {osa-alue2, lukumäärä}... } , {tilasto2}, {tilasto3}...}
        private object[][] Tilastot;

        public string annaVuosi() { return Vuosi; }
        public int annaNeljannes() { return Neljannes; }



        //konstruktori
        public Tyollisyystilasto(object[][] osa_alueet, int vuosi, int neljannes)
        {
            Tilastot = osa_alueet;
            Vuosi = Convert.ToString(vuosi);
            Neljannes = neljannes;
        }
        
        //konstruktori
        public Tyollisyystilasto()
        {
            Console.WriteLine("Työllisyystilaston luominen alkaa...");
            
            bool jatka = true;

            while (jatka)
            {

                Console.Write("Määritä vuosi: ");
                string vuosi = Console.ReadLine();

                try
                {
                    Vuosi = vuosi;
                    jatka = false;
                }
                catch (FormatException err)
                {
                    Console.WriteLine("Virhe! Ei hyväksyttävä vuosityyppi");
                }
            }

            jatka = true;

            while (jatka) { 
                try
                {
                    Console.Write("Määritä neljännes: ");
                    string neljannes = Console.ReadLine();
                    int tmp_neljannes = Convert.ToInt32(neljannes);

                    if(tmp_neljannes < 0 || tmp_neljannes > 100)
                    {
                        throw new FormatException();
                    }

                    Neljannes = tmp_neljannes;
                    jatka = false;
                }
                catch (FormatException err)
                {
                    Console.WriteLine("Virhe! Ei hyväksyttävä neljannes. 1..");
                }
            }

            jatka = true;


            object[] osa_aikaiset = new object[] { osaAlueet.Osa_aikaiset, kasitteleLukumaara(osaAlueet.Osa_aikaiset) };
            object[] maaraaikaiset = new object[] { osaAlueet.Määräaikaisia, kasitteleLukumaara(osaAlueet.Määräaikaisia) };
            object[] vaikeat = new object[] { osaAlueet.Vaikeasti_täytettäviä, kasitteleLukumaara(osaAlueet.Vaikeasti_täytettäviä) };

            Tilastot = new object[][] { maaraaikaiset, osa_aikaiset, vaikeat };
            Console.WriteLine($"Luotiin tilasto vuodelle {Vuosi}, neljännes {Neljannes}");
        }




        private long kasitteleLukumaara(osaAlueet osaalue)
        {
            bool jatka = true;
            long luku = 0;

            while (jatka)
            {
                try
                {
                    Console.WriteLine("Määritä työllistyminen osa-alueessa: " + osaalue);
                    string lukumaara = Console.ReadLine();
                    luku = Convert.ToInt64(lukumaara);

                    if (luku < 0)
                    {
                        throw new FormatException("Luku ei voi olla negatiivinen");
                    }

                    jatka = false;
                    
                }
                catch (FormatException err)
                {
                    Console.WriteLine("Virhe! Ei hyväksyttävä lukumäärä");
                    Console.WriteLine(err.Message);
                }
            }

            return luku;
        }

        //laske paljonko avoimia työpaikkoja oli yhteensä
        public long avoimetTyopaikat()
        {
            long lukumaara = 0;

            foreach(object[] tilasto in Tilastot)
            {
                try
                {
                    lukumaara += Convert.ToInt32(tilasto[1]);
                }
                catch (FormatException)
                {
                    Console.WriteLine("Virhe! Annettua arvoa ei voitu muuttaa luvuksi");
                }
            }

            return lukumaara;
        }

        //hae tietyn osa-alueen tiedot [nimi, työpaikkojen lukumäärä]
        public object[] annaTilasto(osaAlueet osaalue)
        {
            try { 
                object[] item = Tilastot.First(x =>  (osaAlueet)x[0] == osaalue);
                return item;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            
        }
        

        public override string ToString()
        {
            return $"Tilasto {Vuosi} neljannes {Neljannes}, Avoimia työpaikkoja oli {avoimetTyopaikat()} kpl";
        }
    }
}
