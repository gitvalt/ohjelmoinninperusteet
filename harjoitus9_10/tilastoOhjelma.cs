﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus9_10
{
    class tilastoOhjelma
    {
        private List<Tyollisyystilasto> tilastot = new List<Tyollisyystilasto>();


        //konstruktori
        public tilastoOhjelma() {  }

        public void lisaaTilasto(Tyollisyystilasto tilasto)
        {
            tilastot.Add(tilasto);
        }

        //käyttäjältä pyydetään täyttämään tiedot
        public void lisaaTilasto()
        {
            Tyollisyystilasto tilasto = new Tyollisyystilasto();
            tilastot.Add(tilasto);
        }

        public Tyollisyystilasto annaTilasto(int vuosi, int neljannes)
        {
            try
            {
               Tyollisyystilasto tilasto = tilastot.First(item => item.annaVuosi() == Convert.ToString(vuosi) && item.annaNeljannes() == neljannes);
                return tilasto;
            }
            catch (OutOfMemoryException)
            {
                throw new Exception("Tilastoa ei löydetty");
            }
        }

        public void poistaTilasto(int vuosi, int neljannes)
        {
            try
            {
                Tyollisyystilasto tilasto = tilastot.First(item => item.annaVuosi() == Convert.ToString(vuosi) && item.annaNeljannes() == neljannes);
                tilastot.Remove(tilasto);
            }
            catch (OutOfMemoryException)
            {
                throw new Exception("Tilastoa ei löydetty");
            }
        }
       
        //hae tietyn osaalueen keskiarvo
        public long haeKeskiarvo(Tyollisyystilasto.osaAlueet osa)
        {
            long summa = 0;
            int lukumaara = 0;

            try { 
                foreach(Tyollisyystilasto tilasto in tilastot)
                {
                    summa += Convert.ToInt64(tilasto.annaTilasto(osa)[1]);
                    lukumaara++;
                }

                return (summa / lukumaara);

            }
            catch (DivideByZeroException)
            {
                throw new DivideByZeroException("Tilastoja ei ole määriteltynä");
            }
}

        //tulostaa tilastokoneen sisältämien tilastojen yhteenvedon.
        public void tulostaTilastoData()
        {

            
            Console.Write(String.Format("|{0,2}", " Vuosi, neljännes"));

            //kirjoitetaan otsikot
            foreach (Tyollisyystilasto.osaAlueet item in Enum.GetValues(typeof(Tyollisyystilasto.osaAlueet)))
            {
                Console.Write(String.Format(" |{0, 2}", item));
            }
            Console.Write(String.Format(" |{0,2}|", "Kokonaismäärä"));
            Console.Write("\n");
            
            //Käydään läpi jokainen rivi
            foreach (Tyollisyystilasto tila in tilastot)
            {
                string tmp = tila.annaVuosi() + ", " + tila.annaNeljannes();
 
                Console.Write(String.Format("|{0,18}", tmp));
                //käydään läpi jokainen sarake

                int luku = 13;

                foreach (Tyollisyystilasto.osaAlueet item in Enum.GetValues(typeof(Tyollisyystilasto.osaAlueet)))
                {
                    switch (item)
                    {
                        case Tyollisyystilasto.osaAlueet.Osa_aikaiset:
                            Console.Write(String.Format("|{0, " + luku + "}", tila.annaTilasto(item)[1]));
                            luku = 18;
                            break;
                        case Tyollisyystilasto.osaAlueet.Määräaikaisia:
                            Console.Write(String.Format("|{0, " + luku + "}", tila.annaTilasto(item)[1]));
                            luku = 18;
                            break;
                        case Tyollisyystilasto.osaAlueet.Vaikeasti_täytettäviä:
                            Console.Write(String.Format("|{0, " + luku + "}", tila.annaTilasto(item)[1]));
                            luku = 13;
                            break;
                    }
                }
                Console.Write(String.Format("|{0,18}", tila.avoimetTyopaikat()));
                Console.Write("|\n");

            }
            
        }
    }
}
