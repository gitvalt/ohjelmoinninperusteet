﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus9_10
{
    class Program
    {
        static void Main(string[] args)
        {
            //testidatan lähde
            //http://www.stat.fi/til/atp/2017/01/atp_2017_01_2017-05-17_tie_001_fi.html
           
            object[] t1 = new object[] { Tyollisyystilasto.osaAlueet.Osa_aikaiset, 9700 };
            object[] t2 = new object[] { Tyollisyystilasto.osaAlueet.Määräaikaisia, 27800 };
            object[] t3 = new object[] { Tyollisyystilasto.osaAlueet.Vaikeasti_täytettäviä, 13400 };
            
            //luodaan ensimmäisen neljänneksen tiedot
            object[][] til2016 = { t1, t2, t3};

            object[] g1 = new object[] { Tyollisyystilasto.osaAlueet.Osa_aikaiset, 13000 };
            object[] g2 = new object[] { Tyollisyystilasto.osaAlueet.Määräaikaisia, 33200 };
            object[] g3 = new object[] { Tyollisyystilasto.osaAlueet.Vaikeasti_täytettäviä, 16900 };

            //luodaan seuraavan neljänneksen tiedot
            object[][] til2017 = { g1, g2, g3 };

            Tyollisyystilasto tilasto_1 = null, tilasto_2 = null;
            tilastoOhjelma tilastoK = null;

            try
            { 
                //Luodaan kaksi tilastoa ja testataan tilaston tulostusta
                tilasto_1 = new Tyollisyystilasto(til2016, 2016, 1);
                tilasto_2 = new Tyollisyystilasto(til2017, 2017, 1);
                Console.WriteLine(tilasto_1.ToString());

                //luodaan tilastokone ja lisätään luodut tilastot
                tilastoK = new tilastoOhjelma();
                tilastoK.lisaaTilasto(tilasto_1);
                tilastoK.lisaaTilasto(tilasto_2);
            }
            catch (Exception err)
            {
                //käsitellään virheet.
                Console.WriteLine("Tapahtui virhe objektien luomisen kanssa.");
                Console.WriteLine(err.Message);
            }

            Console.WriteLine("Tulostetaan nykyinen tilanne:");

            //tarkistetaan nykyinen tilanne
            tilastoK.tulostaTilastoData();

            Console.WriteLine("Luodaan uusi tilasto");

            Console.WriteLine("Tilastokokoelman määräaikaisten työpaikkojen keskiarvo: " + tilastoK.haeKeskiarvo(Tyollisyystilasto.osaAlueet.Määräaikaisia));

            //Käyttäjä luo uuden tilaston
            tilastoK.lisaaTilasto();

            tilastoK.tulostaTilastoData();

            //Päätetään ohjelma...
            Console.WriteLine("Ohjelma paattyy...");
            Console.ReadLine();
            
        }
    }
}
