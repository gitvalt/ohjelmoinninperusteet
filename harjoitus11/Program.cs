﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus11
{
    class Program
    {
        static void Main(string[] args)
        {
            LINQWithSimpleTypeArray program = new LINQWithSimpleTypeArray();
        }
    }

    class LINQWithSimpleTypeArray
    {
        public LINQWithSimpleTypeArray()
        {
            // luodaan luku array
            var luvut = new[] { 2, 9, 5, 0, 3, 7, 1, 4, 8, 5 };

            // tulostetaan alkuperäiset arvot 
            Console.Write("Alkuperäinen lukujono:");
            foreach (var elementti in luvut)
            {
                Console.Write($" {elementti}");
            }

            // LINQ kysely, jolla haetaan arvoja jotka ovat suurempia 4 
            var suodatettu =
            from luku in luvut // määritetään tiedon lähteeksi  
            where luku > 4
            select luku;

            // tulostetaan suodatetu luvut
            Console.Write("\nLuvut jotka ovat suurempia kuin 4:");
            foreach (var elementti in suodatettu)
            {
                Console.Write($" {elementti}");
            }

            //käytetään lajittele käskyä alkuperäisten arvojen lajitteluun kasvavaan järjestykseen
            var lajiteltu =
            from luku in luvut // datan lähteeksi määritellään "luvut" 
            orderby luku
            select luku;

            // näytetään lajiteltu lista
            Console.Write("\nAlkuperäinen lista, lajiteltuna:");
            foreach (var elementti in lajiteltu)
            {
                Console.Write($" {elementti}");
            }

            //lajittele suoratetut tuloksen laskevaan järjestykseen
            var lajiteltuSuodatettu =
            from luku in suodatettu// datan lähde on LINQ kyselyllä suodatettu data. 
            orderby luku descending
            select luku;

            // Näytä lajiteltu lista 
            Console.Write(
            "\nArvo suurempi kuin 4, laskevaan järjestykseen (kaksi kyselyä):");
            foreach (var elementti in lajiteltuSuodatettu)
            {
                Console.Write($" {elementti}");
            }

            //suodata alkuperäinen lista ja lajittele saadut arvot laskevaan järjestykseen
            var suodataJaLajittele =
            from luku in luvut // data source is values 
            where luku > 4
            orderby luku descending
            select luku;

            // display the filtered and sorted results 
            Console.Write(
            "\nArvot suurempia kuin 4, laskeva järjestys (yksi kysely):");
            foreach (var elementti in suodataJaLajittele)
            {
                Console.Write($" {elementti}");
            }

            //Oma koodi alkaa
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nOma koodi alkaa...");
            Console.ResetColor();

            Console.Write("Määritelty oma lista: ");
            var lukuja = new[] { 111, 79, 645, 11111, 2, 77, 63791 };

            foreach (var luku in lukuja)
            {
                Console.Write(luku + " ");
            }

            //erotin
            Console.WriteLine();
            int haluttuRajoitin = 600;

            var suodatettuOma =
                from luku in lukuja
                where luku > haluttuRajoitin
                select luku;

            Console.Write($"Määritelty suodatettu lista rajoittimella \"{haluttuRajoitin}\": ");

            foreach (var luku in suodatettuOma)
            {
                Console.Write(luku + " ");
            }

            //erotin
            Console.WriteLine();

            var lajiteltuOma =
                from luku in lukuja
                orderby luku descending
                select luku;

            Console.Write("Lajiteltu oma lista: ");

            foreach (var luku in lajiteltuOma)
            {
                Console.Write(luku + " ");
            }

            //erotin
            Console.WriteLine();

            var kobinaatio =
                from luku in lukuja
                orderby luku descending
                where luku < haluttuRajoitin
                select luku;

            Console.Write("Kahden edellisen tehtävät kombinaatio: ");

            foreach (var luku in kobinaatio)
            {
                Console.Write(luku + " ");
            }

            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
