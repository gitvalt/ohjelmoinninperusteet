﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OhjelmoinninPerusteet_harjoitus2
{
    class Program
    {

        static private List<string> albums = new List<string>();
        static private bool shutDown = false;
        private static int state;

        static void Main(string[] args)
        {
            state = 0;
            
            //intro and mainmenu
            Console.WriteLine("Welcome to album application!");
            Console.WriteLine("Your options:");
            
            //setting up test values
            albums.Add("Album X");
            albums.Add("Album Y");
            albums.Add("Album Z");

            while (shutDown == false)
            {

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"state: {state}");
                Console.ResetColor();

                switch (state)
                {
                    case 1:
                        //Show list of albums in the application
                        readAlbums();
                        showMenu();
                        break;
                    case 2:
                        //add album to list
                        AddToList();
                        showMenu();
                        break;
                    case 3:
                        //remove album from list
                        RemoveFromList();
                        showMenu();
                        break;
                    case 4:
                        //sum calculator
                        PriceCalculator();
                        break;
                    case 5:
                        //shutdown the application
                        shutDown = true;
                        break;
                    default:
                        showMenu();
                        break;
                }
            }
            Console.WriteLine("Shutting down");
            Console.ReadLine();
        }

        public static void readAlbums()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            foreach (var album in albums)
            {
                Console.WriteLine(album);
            }
            Console.ResetColor();
        }

        public static void showMenu()
        {
            string options = "1. List albums\n" +
                        "2. Add albums\n" +
                        "3. Remove albums\n" +
                        "4. Calculator\n" +
                        "5. Exit program";

            Console.WriteLine(options);
            state = readInput();
        }

        public static int readInput()
        {
            //read and parse user menu selection
            string input = Console.ReadLine();

            if (int.TryParse(input, out int results) == true)
            {
                int inputParsed = int.Parse(input);
                Console.WriteLine($"You selected option: {input}");

                if (inputParsed >= 0 && inputParsed <= 5)
                {
                    return inputParsed;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Selection out of range!");
                    Console.ResetColor();
                }

            }
            else
            {
                Console.WriteLine($"Your input was not a int: {input}");
            }
            return 0;
        }

        public static void AddToList()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Name of album: ");
            string albumName = Console.ReadLine();
            Console.ResetColor();

            //Does album already exist?
            if (albums.Contains(albumName))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error. Album already exists!");
                Console.ResetColor();
            } else
            {
                albums.Add(albumName);
                Console.WriteLine("Success");
            }
        }

        public static void RemoveFromList()
        {
            readAlbums();
            
            bool cont_query = true;

            while(cont_query == true)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Album to remove: (write !quit to exit)");
                string albumName = Console.ReadLine();
                Console.ResetColor();

                if(albumName == "!quit")
                {
                    cont_query = false;
                    break;
                } else
                { 

                    if (!albums.Contains(albumName))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("No album with that name");
                        Console.ResetColor();
                    }
                    else
                    {
                        var item = albums.First(alb => alb == albumName);
                        albums.Remove(item);
                        cont_query = false;
                    }
                }
            }

            

        }

        public static void PriceCalculator()
        {
            string value1 = "";
            string value2 = "";
            float result;

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Give first value");
            value1 = Console.ReadLine();

            while (float.TryParse(value1, out float results) == false)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error! Could not parse value into a float");
                value1 = Console.ReadLine();
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Give second value");
            value2 = Console.ReadLine();

            while (float.TryParse(value2, out float results) == false)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error! Could not parse value into a float");
                value1 = Console.ReadLine();
            }
            bool continueOperation = true;
            int option = 0;

            while (continueOperation)
            {
                Console.WriteLine($"Values given: {value1} and {value2}");
                Console.WriteLine("Select operation: \n" +
                    "0 Sum\n" +
                    "1 Difference\n" +
                    "2 Multiply\n" +
                    "3 Divide \n" +
                    "4 Exit");
                option = int.Parse(Console.ReadLine());
                switch (option)
                {
                    case 0:
                        result = float.Parse(value1) + float.Parse(value2);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Sum: " + result);
                        break;
                    case 1:
                        result = float.Parse(value1) - float.Parse(value2);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Difference: " + result);
                        break;
                    case 2:
                        result = float.Parse(value1) * float.Parse(value2);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Multiply: " + result);
                        break;
                    case 3:
                        result = float.Parse(value1) / float.Parse(value2);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Division: " + result);
                        break;
                    case 4:
                        continueOperation = false;
                        state = 0;
                        break;
                    default:
                        break;
                }
                Console.ResetColor();
            }
        }

    }
}
