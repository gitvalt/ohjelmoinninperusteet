Ohjelmoinnin perusteet harjoituksia

--- harjoitus 2
Perus albumlista ohjelmisto. Voidaan lukea, list�t� ja poistaa albumin nimi�. Lis�ksi on laskin, joka k�ytt�� aritmeettisi� operaatiota lukujen tulosten laskemiseksi.

-- harjoitus 3
Luotiin perus tilinhallinta ohjelma windows konsolin avulla. Voidaan lis�t� tilille rahaa, n�hd� tilin tila.

-- harjoitus 5
Luotiin UML-kaavio ja kehitettiin ohjelma sen perusteella.

-- harjoitus 6
Luodaan ohjelma UML-kaavion mukaan

-- harjoitus 7
Analysoidaan ohjelma, luodaan UML-kaavio, kommentoidaan ohjelma.

-- harjoitus 8
Erilaisten metodien luomista

-- harjoitus 9 - 10
Taulukoiden testaamista k�ytt�m�ll� valittua tilastoa. Virheiden k�sittelyn testaamista

-- harjoitus 11
LINQ harjoittelua

-- harjoitus 12 - 13
Erilaisten list<> listojen metodien testaamista.

-- harjoitus 14
P�iv�m��rien ja aikoijen hallintaa

-- harjoitus 15
Windows form application




