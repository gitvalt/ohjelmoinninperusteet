﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace harjoitus15
{
    public partial class mainMenu : Form
    {
        public mainMenu()
        {
            InitializeComponent();

            userManager manager = new userManager();
            HolidayReader reader = new HolidayReader();

            //muistia luetaan vain jos lomamatkoja ja käyttäjää ei olla määriteltynä
            if(manager.getUser() == null && reader.getHolidays().Count == 0) { 
                manager.readXmlMemory();
                reader.readXmlMemory();
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            browseDestinations form = new browseDestinations();
            form.Show(this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            profileCreation form = new profileCreation();
            form.Show(this);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            showHolidays form = new showHolidays();
            form.Show(this);
        }
    }
}
