﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace harjoitus15
{
    public partial class OrderingTravels_2 : Form
    {

        List<string> errorMessages = new List<string>();

        //konstruktori
        public OrderingTravels_2()
        {
            InitializeComponent();

            //get user
            userManager manager = new userManager();
            User person = manager.getUser();

            string errorMsg = "Ei määritelty";

           if(person.Firstname != null)
            {
                userName_label.Text = person.Firstname + " " + person.Lastname;
            }
           else
            {
                userName_label.Text = errorMsg;
            }

           if(person.Phone != null) { 
                userPhone_box.Text = person.Phone;
            }
            else
            {
                userPhone_box.Text = errorMsg;
            }

           if(person.Email != null) { 
                userEmail_box.Text = person.Email;
            } else
            {
                userEmail_box.Text = errorMsg;
            }

            HolidayReader reader = new HolidayReader();

            foreach(Holiday item in reader.getHolidays())
            {
                listBox1.Items.Add($"{item.Title}, {item.Description}, {item.Destination}, {item.PricePerDay}€/päivä");
            }

        }

        private bool checkValues()
        {
            //tyhjennä virheilmoitus lista
            errorMessages.Clear();
            ErrorMessages.Text = "";

            DateTime start = alkamispaiva_picker.Value;
            DateTime end = paattymispaiva_picker.Value;


            //jos tapahtuma alkaa loppumisajan jälkeen
            switch (DateTime.Compare(start, end))
            {
                case -1:
                    break;
                case 0:
                    errorMessages.Add("Tapahtuma alkaa samana päivänä kuin tapahtuma päättyy");
                    break;
                case 1:
                    errorMessages.Add("Tapahtuma alkaa loppumisajan jälkeen");
                    break;
            }

            TimeSpan spanOfTime = end - start;

            if (spanOfTime.Days == 0)
            {
                errorMessages.Add("Matkan kesto on 0 päivää");
            }

            if(listBox1.SelectedItems.Count == 0)
            {
                //valintoja ei olla tehtynä
                errorMessages.Add("Lomamatka tyyppiä ei olla valittuna");
            }
            
            //tapahtuiko virheitä?
            if(errorMessages.Count == 0)
            {
                //ei
                HolidayReader reader = new HolidayReader();

                //haetaan otsikkoa valitusta lomamatkasta
                List<string> title = listBox1.SelectedItem.ToString().Split(',').ToList();

                Holiday selected = reader.getHolidays().First(item => item.Title == title[0]);

                HolidayTrip trip = new HolidayTrip(selected, start, end);

                return true;
            }
            else
            {
                //kyllä
                return false;
            }

        }

        private void printErrors()
        {
            foreach(string error in errorMessages)
            {
                ErrorMessages.Text += error + "\n";
            }
        }

        //tallenna uusi lomamatka
        private void button2_Click(object sender, EventArgs e)
        {
            //tarkista annetut tiedot
            if(checkValues() == false)
            {
                printErrors();
            }
            else
            {
                HolidayReader reader = new HolidayReader();
                userManager manager = new userManager();

                //haetaan otsikkoa valitusta lomamatkasta
                List<string> title = listBox1.SelectedItem.ToString().Split(',').ToList();

                Holiday selected = reader.getHolidays().First(item => item.Title == title[0]);
                DateTime start = alkamispaiva_picker.Value;
                DateTime end = paattymispaiva_picker.Value;

                HolidayTrip trip = new HolidayTrip(selected, start, end);
                manager.getUser().addHoliday(trip);

                confirmDialogcs window = new confirmDialogcs();
                window.Show(this.Owner);
                this.Close();
            }

        }

        //peruuta
        private void button1_Click(object sender, EventArgs e)
        {
            profileCreation form = new profileCreation();
            form.Show(this.Owner);
            this.Close();
        }

        //takaisin etusivulle
        private void button3_Click(object sender, EventArgs e)
        {
            this.Owner.Visible = true;
            this.Close();
        }
    }
}
