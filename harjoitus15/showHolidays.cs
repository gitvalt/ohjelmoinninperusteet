﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace harjoitus15
{
    public partial class showHolidays : Form
    {
        public showHolidays()
        {
            InitializeComponent();

            userManager manager = new userManager();
            User person = manager.getUser();

            if(person.Orderedholidays.Count == 0)
            {
                displayerHolidayTrips.Rows.Add("Matkoja ei määriteltyn");
            } else
            {
                foreach(HolidayTrip trip in person.Orderedholidays)
                {
                    displayerHolidayTrips.Rows.Add(trip.holidayDestination.Title, trip.startDate.ToString("d.m.yyyy"), 
                        trip.endDate.Date.ToString("d.m.yyyy"), trip.holidayDestination.PricePerDay + "€", trip.priceTotal + "€");
                }
            }

        }

        private void Takaisin_Click(object sender, EventArgs e)
        {
            this.Owner.Visible = true;
            this.Close();
        }
    }
}
