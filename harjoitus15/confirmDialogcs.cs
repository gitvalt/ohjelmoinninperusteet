﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace harjoitus15
{
    public partial class confirmDialogcs : Form
    {
        public confirmDialogcs()
        {
            InitializeComponent();
            userManager manager = new userManager();
            HolidayTrip newHoliday = manager.getUser().Orderedholidays.Last();
            InfoBox.Text = $"Lomamatka: {newHoliday.holidayDestination.Title}\n" +
                $"Matkan aikataulu: {newHoliday.startDate.ToString("d.m.yyyy")} - {newHoliday.endDate.ToString("d.m.yyyy")}\n" +
                $"Matkan kesto: {newHoliday.durationInDays()} päivää\n" +
                $"Kokonaishinta: {newHoliday.priceTotal}€";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Owner.Visible = true;
            this.Close();
        }
    }
}
