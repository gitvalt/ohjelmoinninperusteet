﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace harjoitus15
{
    public partial class profileCreation : Form
    {
        //lista virheistä
        private List<string> errorMsg = new List<string>();

        public profileCreation()
        {
            InitializeComponent();

            //hae olemassa oleva käyttäjä
            userManager user = new userManager();
            User userInMemory = user.getUser();
            string firstname = userInMemory.Firstname;

            //onko käyttäjä muistissa
            if (firstname == "" || firstname.ToLower() == "undefined")
            {
                //ei ole

            }
            else
            {
                //on olemassa. Täytetään tiedot muistista

                box_firstname.Text = firstname;
                box_lastname.Text = userInMemory.Lastname;
                box_email.Text = userInMemory.Email;
                box_phone.Text = userInMemory.Phone;

                switch (userInMemory.userGenre)
                {
                    case User.genre.man:
                        genreMan.Checked = true;
                        break;

                    case User.genre.woman:
                        genreWoman.Checked = true;
                        break;

                    case User.genre.other:
                        genre_other.Checked = true;
                        break;

                    default:
                        //undefined value
                        break;
                }

            }
            
        }
        
        //käsitellään annetut tiedot
        private bool handleSubmit()
        {
            string firstname = "", lastname = "", phone = "", email = "";

            //tyhjennettään virhelista
            errorMsg.Clear();
            ErrorMessages.Text = "";

            //onko etunimi validi
            if(box_firstname.Text == "")
            {
                info_firstname.ForeColor = Color.Red;
                errorMsg.Add("Etunimi ei voi olla tyhjä");
            }
            else
            {
                info_firstname.ForeColor = Color.Black;
                firstname = box_firstname.Text;
            }

            //onko sukunimi validi
            if (box_lastname.Text == "")
            {
                info_lastname.ForeColor = Color.Red;
                errorMsg.Add("Sukunimi ei voi olla tyhjä");
            }
            else
            {
                info_lastname.ForeColor = Color.Black;
                lastname = box_lastname.Text;
            }
            
            //onko sähköposti validi
            if (box_email.Text == "")
            {
                info_email.ForeColor = Color.Red;
                errorMsg.Add("Sähköposti ei voi olla tyhjä");
            }
            else
            {
                info_email.ForeColor = Color.Black;
                email = box_email.Text;
            }
            
            //onko puhelinnumero validi
            if (box_phone.Text == "")
            {
                info_phone.ForeColor = Color.Red;
                errorMsg.Add("Puhelinnumero ei voi olla tyhjä");
            }
            else
            {
                info_phone.ForeColor = Color.Black;
                phone = box_phone.Text;
            }

            //luodaan käyttäjä saatujen tietojen perusteella
            User newUser = new User(firstname, lastname, email, phone);
            

            //tarkista valittu sukupuoli
            if (genreWoman.Checked == true)
            {
                newUser.setGenre(User.genre.woman);
            }
            else if(genreMan.Checked == true)
            {
                newUser.setGenre(User.genre.man);
            }
            else if(genre_other.Checked == true)
            {
                newUser.setGenre(User.genre.other);
            }
            else
            {
                sukupuoli_info.ForeColor = Color.Red;
                errorMsg.Add("Sukupuolta ei määritelty");
            }


            //jos virheitä ei havaittu
            if (errorMsg.Count == 0)
            {
                //luodaan käyttäjä
                userManager manager = new userManager();
                manager.setNewUser(newUser);
                return true;
            }
            else
            {
                //jos virheitä on havaittu
                return false;
            }
        }

        private void printErrorMessages()
        {
            foreach(string error in errorMsg)
            {
                ErrorMessages.Text += error + "\n";
            }
        }

        //palaa etusivulle  
        private void button1_Click(object sender, EventArgs e)
        {
            this.Owner.Visible = true;
            this.Close();
        }
        
        //siirry tilaukseen
        private void button2_Click(object sender, EventArgs e)
        {
            if(handleSubmit() == true) {
                OrderingTravels_2 form = new OrderingTravels_2();
                form.Show(this.Owner);
                this.Close();
            }
            else
            {
                //tulostetaan virheilmoitukset
                printErrorMessages();
            }

        }
    }
}
