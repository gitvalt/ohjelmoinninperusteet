﻿namespace harjoitus15
{
    partial class profileCreation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.box_firstname = new System.Windows.Forms.TextBox();
            this.info_firstname = new System.Windows.Forms.Label();
            this.info_lastname = new System.Windows.Forms.Label();
            this.box_lastname = new System.Windows.Forms.TextBox();
            this.info_email = new System.Windows.Forms.Label();
            this.box_email = new System.Windows.Forms.TextBox();
            this.info_phone = new System.Windows.Forms.Label();
            this.box_phone = new System.Windows.Forms.TextBox();
            this.genreMan = new System.Windows.Forms.RadioButton();
            this.genreWoman = new System.Windows.Forms.RadioButton();
            this.sukupuoli_info = new System.Windows.Forms.Label();
            this.button_toBack = new System.Windows.Forms.Button();
            this.button_toNext = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.genre_other = new System.Windows.Forms.RadioButton();
            this.ErrorMessages = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // box_firstname
            // 
            this.box_firstname.Location = new System.Drawing.Point(118, 64);
            this.box_firstname.Name = "box_firstname";
            this.box_firstname.Size = new System.Drawing.Size(187, 20);
            this.box_firstname.TabIndex = 0;
            // 
            // info_firstname
            // 
            this.info_firstname.AutoSize = true;
            this.info_firstname.Location = new System.Drawing.Point(27, 67);
            this.info_firstname.Name = "info_firstname";
            this.info_firstname.Size = new System.Drawing.Size(41, 13);
            this.info_firstname.TabIndex = 1;
            this.info_firstname.Text = "Etunimi";
            // 
            // info_lastname
            // 
            this.info_lastname.AutoSize = true;
            this.info_lastname.Location = new System.Drawing.Point(27, 107);
            this.info_lastname.Name = "info_lastname";
            this.info_lastname.Size = new System.Drawing.Size(50, 13);
            this.info_lastname.TabIndex = 3;
            this.info_lastname.Text = "Sukunimi";
            // 
            // box_lastname
            // 
            this.box_lastname.Location = new System.Drawing.Point(118, 104);
            this.box_lastname.Name = "box_lastname";
            this.box_lastname.Size = new System.Drawing.Size(187, 20);
            this.box_lastname.TabIndex = 1;
            // 
            // info_email
            // 
            this.info_email.AutoSize = true;
            this.info_email.Location = new System.Drawing.Point(27, 148);
            this.info_email.Name = "info_email";
            this.info_email.Size = new System.Drawing.Size(60, 13);
            this.info_email.TabIndex = 5;
            this.info_email.Text = "Sähköposti";
            // 
            // box_email
            // 
            this.box_email.Location = new System.Drawing.Point(118, 145);
            this.box_email.Name = "box_email";
            this.box_email.Size = new System.Drawing.Size(187, 20);
            this.box_email.TabIndex = 2;
            // 
            // info_phone
            // 
            this.info_phone.AutoSize = true;
            this.info_phone.Location = new System.Drawing.Point(27, 190);
            this.info_phone.Name = "info_phone";
            this.info_phone.Size = new System.Drawing.Size(42, 13);
            this.info_phone.TabIndex = 7;
            this.info_phone.Text = "Puhelin";
            // 
            // box_phone
            // 
            this.box_phone.Location = new System.Drawing.Point(118, 187);
            this.box_phone.Name = "box_phone";
            this.box_phone.Size = new System.Drawing.Size(187, 20);
            this.box_phone.TabIndex = 3;
            // 
            // genreMan
            // 
            this.genreMan.AutoSize = true;
            this.genreMan.Location = new System.Drawing.Point(30, 287);
            this.genreMan.Name = "genreMan";
            this.genreMan.Size = new System.Drawing.Size(47, 17);
            this.genreMan.TabIndex = 4;
            this.genreMan.TabStop = true;
            this.genreMan.Text = "Mies";
            this.genreMan.UseVisualStyleBackColor = true;
            // 
            // genreWoman
            // 
            this.genreWoman.AutoSize = true;
            this.genreWoman.Location = new System.Drawing.Point(118, 287);
            this.genreWoman.Name = "genreWoman";
            this.genreWoman.Size = new System.Drawing.Size(59, 17);
            this.genreWoman.TabIndex = 5;
            this.genreWoman.TabStop = true;
            this.genreWoman.Tag = "sukupuoli";
            this.genreWoman.Text = "Nainen";
            this.genreWoman.UseVisualStyleBackColor = true;
            // 
            // sukupuoli_info
            // 
            this.sukupuoli_info.AutoSize = true;
            this.sukupuoli_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sukupuoli_info.Location = new System.Drawing.Point(27, 246);
            this.sukupuoli_info.Name = "sukupuoli_info";
            this.sukupuoli_info.Size = new System.Drawing.Size(79, 20);
            this.sukupuoli_info.TabIndex = 10;
            this.sukupuoli_info.Text = "Sukupuoli";
            // 
            // button_toBack
            // 
            this.button_toBack.Location = new System.Drawing.Point(24, 413);
            this.button_toBack.Name = "button_toBack";
            this.button_toBack.Size = new System.Drawing.Size(116, 38);
            this.button_toBack.TabIndex = 6;
            this.button_toBack.Text = "Peruuta";
            this.button_toBack.UseVisualStyleBackColor = true;
            this.button_toBack.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_toNext
            // 
            this.button_toNext.Location = new System.Drawing.Point(250, 413);
            this.button_toNext.Name = "button_toNext";
            this.button_toNext.Size = new System.Drawing.Size(116, 38);
            this.button_toNext.TabIndex = 7;
            this.button_toNext.Text = "Seuraava";
            this.button_toNext.UseVisualStyleBackColor = true;
            this.button_toNext.Click += new System.EventHandler(this.button2_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(26, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 24);
            this.label8.TabIndex = 13;
            this.label8.Text = "Asiakas";
            // 
            // genre_other
            // 
            this.genre_other.AutoSize = true;
            this.genre_other.Location = new System.Drawing.Point(207, 287);
            this.genre_other.Name = "genre_other";
            this.genre_other.Size = new System.Drawing.Size(46, 17);
            this.genre_other.TabIndex = 14;
            this.genre_other.TabStop = true;
            this.genre_other.Tag = "sukupuoli";
            this.genre_other.Text = "Muu";
            this.genre_other.UseVisualStyleBackColor = true;
            // 
            // ErrorMessages
            // 
            this.ErrorMessages.AutoSize = true;
            this.ErrorMessages.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ErrorMessages.Location = new System.Drawing.Point(24, 343);
            this.ErrorMessages.Name = "ErrorMessages";
            this.ErrorMessages.Size = new System.Drawing.Size(0, 13);
            this.ErrorMessages.TabIndex = 15;
            // 
            // profileCreation
            // 
            this.ClientSize = new System.Drawing.Size(408, 503);
            this.Controls.Add(this.ErrorMessages);
            this.Controls.Add(this.genre_other);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button_toNext);
            this.Controls.Add(this.button_toBack);
            this.Controls.Add(this.sukupuoli_info);
            this.Controls.Add(this.genreWoman);
            this.Controls.Add(this.genreMan);
            this.Controls.Add(this.info_phone);
            this.Controls.Add(this.box_phone);
            this.Controls.Add(this.info_email);
            this.Controls.Add(this.box_email);
            this.Controls.Add(this.info_lastname);
            this.Controls.Add(this.box_lastname);
            this.Controls.Add(this.info_firstname);
            this.Controls.Add(this.box_firstname);
            this.Name = "profileCreation";
            this.Text = "Asiakkaan profiilin asetukset";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox holidayCheckbox;
        private System.Windows.Forms.TextBox box_firstname;
        private System.Windows.Forms.Label info_firstname;
        private System.Windows.Forms.Label info_lastname;
        private System.Windows.Forms.TextBox box_lastname;
        private System.Windows.Forms.Label info_email;
        private System.Windows.Forms.TextBox box_email;
        private System.Windows.Forms.Label info_phone;
        private System.Windows.Forms.TextBox box_phone;
        private System.Windows.Forms.RadioButton genreMan;
        private System.Windows.Forms.RadioButton genreWoman;
        private System.Windows.Forms.Label sukupuoli_info;
        private System.Windows.Forms.Button button_toBack;
        private System.Windows.Forms.Button button_toNext;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton genre_other;
        private System.Windows.Forms.Label ErrorMessages;
    }
}