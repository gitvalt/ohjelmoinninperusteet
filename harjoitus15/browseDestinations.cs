﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace harjoitus15
{
    public partial class browseDestinations : Form
    {
        public browseDestinations()
        {
            InitializeComponent();

            //luodaan lukija
            HolidayReader reader = new HolidayReader();

            //tulostetaan listview elementtiin jokainen määritelty lomamatkavaihtoehto
            foreach(Holiday item in reader.getHolidays())
            {
                matkatDisplay.Rows.Add(item.Title, item.Destination, item.Description, item.PricePerDay);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Owner.Visible = true;
            this.Close();
        }
    }
}
