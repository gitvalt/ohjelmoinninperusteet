﻿namespace harjoitus15
{
    partial class OrderingTravels_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.alkamispaiva_picker = new System.Windows.Forms.DateTimePicker();
            this.paattymispaiva_picker = new System.Windows.Forms.DateTimePicker();
            this.alkamispaiva = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Matkakohteet = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.userName_label = new System.Windows.Forms.Label();
            this.userEmail_box = new System.Windows.Forms.Label();
            this.userPhone_box = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button3 = new System.Windows.Forms.Button();
            this.ErrorMessages = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(43, 372);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 32);
            this.button1.TabIndex = 7;
            this.button1.Text = "Peruuta";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(528, 372);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(131, 32);
            this.button2.TabIndex = 8;
            this.button2.Text = "Tallenna";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // alkamispaiva_picker
            // 
            this.alkamispaiva_picker.Location = new System.Drawing.Point(44, 176);
            this.alkamispaiva_picker.Name = "alkamispaiva_picker";
            this.alkamispaiva_picker.Size = new System.Drawing.Size(200, 20);
            this.alkamispaiva_picker.TabIndex = 9;
            // 
            // paattymispaiva_picker
            // 
            this.paattymispaiva_picker.Location = new System.Drawing.Point(44, 295);
            this.paattymispaiva_picker.Name = "paattymispaiva_picker";
            this.paattymispaiva_picker.Size = new System.Drawing.Size(200, 20);
            this.paattymispaiva_picker.TabIndex = 10;
            // 
            // alkamispaiva
            // 
            this.alkamispaiva.AutoSize = true;
            this.alkamispaiva.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alkamispaiva.Location = new System.Drawing.Point(41, 149);
            this.alkamispaiva.Name = "alkamispaiva";
            this.alkamispaiva.Size = new System.Drawing.Size(90, 16);
            this.alkamispaiva.TabIndex = 11;
            this.alkamispaiva.Text = "Alkamispäivä";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 270);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 16);
            this.label1.TabIndex = 12;
            this.label1.Text = "Päättymispäivä";
            // 
            // Matkakohteet
            // 
            this.Matkakohteet.AutoSize = true;
            this.Matkakohteet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Matkakohteet.Location = new System.Drawing.Point(321, 21);
            this.Matkakohteet.Name = "Matkakohteet";
            this.Matkakohteet.Size = new System.Drawing.Size(83, 16);
            this.Matkakohteet.TabIndex = 13;
            this.Matkakohteet.Text = "Matkakohde";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(41, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Käyttäjätiedot";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Nimi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Sähköposti";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Puhelin";
            // 
            // userName_label
            // 
            this.userName_label.AutoSize = true;
            this.userName_label.Location = new System.Drawing.Point(148, 48);
            this.userName_label.Name = "userName_label";
            this.userName_label.Size = new System.Drawing.Size(0, 13);
            this.userName_label.TabIndex = 18;
            // 
            // userEmail_box
            // 
            this.userEmail_box.AutoSize = true;
            this.userEmail_box.Location = new System.Drawing.Point(148, 72);
            this.userEmail_box.Name = "userEmail_box";
            this.userEmail_box.Size = new System.Drawing.Size(0, 13);
            this.userEmail_box.TabIndex = 19;
            // 
            // userPhone_box
            // 
            this.userPhone_box.AutoSize = true;
            this.userPhone_box.Location = new System.Drawing.Point(148, 98);
            this.userPhone_box.Name = "userPhone_box";
            this.userPhone_box.Size = new System.Drawing.Size(0, 13);
            this.userPhone_box.TabIndex = 20;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(323, 57);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(336, 160);
            this.listBox1.TabIndex = 23;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(44, 436);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(131, 32);
            this.button3.TabIndex = 24;
            this.button3.Text = "Takaisin etusivulle";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ErrorMessages
            // 
            this.ErrorMessages.AutoSize = true;
            this.ErrorMessages.Location = new System.Drawing.Point(323, 242);
            this.ErrorMessages.Name = "ErrorMessages";
            this.ErrorMessages.Size = new System.Drawing.Size(0, 13);
            this.ErrorMessages.TabIndex = 25;
            // 
            // OrderingTravels_2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 493);
            this.Controls.Add(this.ErrorMessages);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.userPhone_box);
            this.Controls.Add(this.userEmail_box);
            this.Controls.Add(this.userName_label);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Matkakohteet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.alkamispaiva);
            this.Controls.Add(this.paattymispaiva_picker);
            this.Controls.Add(this.alkamispaiva_picker);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "OrderingTravels_2";
            this.Text = "Matkan tilaus";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker alkamispaiva_picker;
        private System.Windows.Forms.DateTimePicker paattymispaiva_picker;
        private System.Windows.Forms.Label alkamispaiva;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Matkakohteet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label userName_label;
        private System.Windows.Forms.Label userEmail_box;
        private System.Windows.Forms.Label userPhone_box;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label ErrorMessages;
    }
}