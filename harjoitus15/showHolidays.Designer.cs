﻿namespace harjoitus15
{
    partial class showHolidays
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.displayerHolidayTrips = new System.Windows.Forms.DataGridView();
            this.Takaisin = new System.Windows.Forms.Button();
            this.title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endtime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceperday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Totalprice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.displayerHolidayTrips)).BeginInit();
            this.SuspendLayout();
            // 
            // displayerHolidayTrips
            // 
            this.displayerHolidayTrips.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.displayerHolidayTrips.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.title,
            this.startdate,
            this.endtime,
            this.priceperday,
            this.Totalprice});
            this.displayerHolidayTrips.Location = new System.Drawing.Point(25, 29);
            this.displayerHolidayTrips.Name = "displayerHolidayTrips";
            this.displayerHolidayTrips.Size = new System.Drawing.Size(638, 206);
            this.displayerHolidayTrips.TabIndex = 0;
            // 
            // Takaisin
            // 
            this.Takaisin.Location = new System.Drawing.Point(25, 284);
            this.Takaisin.Name = "Takaisin";
            this.Takaisin.Size = new System.Drawing.Size(129, 34);
            this.Takaisin.TabIndex = 1;
            this.Takaisin.Text = "Takaisin";
            this.Takaisin.UseVisualStyleBackColor = true;
            this.Takaisin.Click += new System.EventHandler(this.Takaisin_Click);
            // 
            // title
            // 
            this.title.HeaderText = "Otsikko";
            this.title.Name = "title";
            // 
            // startdate
            // 
            this.startdate.HeaderText = "Alkamisaika";
            this.startdate.Name = "startdate";
            // 
            // endtime
            // 
            this.endtime.HeaderText = "Loppumisaika";
            this.endtime.Name = "endtime";
            // 
            // priceperday
            // 
            this.priceperday.HeaderText = "Hinta per päivä";
            this.priceperday.Name = "priceperday";
            // 
            // Totalprice
            // 
            this.Totalprice.HeaderText = "Kokonaishinta";
            this.Totalprice.Name = "Totalprice";
            // 
            // showHolidays
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 330);
            this.Controls.Add(this.Takaisin);
            this.Controls.Add(this.displayerHolidayTrips);
            this.Name = "showHolidays";
            this.Text = "Näytä tilatut matkat";
            ((System.ComponentModel.ISupportInitialize)(this.displayerHolidayTrips)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView displayerHolidayTrips;
        private System.Windows.Forms.Button Takaisin;
        private System.Windows.Forms.DataGridViewTextBoxColumn title;
        private System.Windows.Forms.DataGridViewTextBoxColumn startdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn endtime;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceperday;
        private System.Windows.Forms.DataGridViewTextBoxColumn Totalprice;
    }
}