﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus15
{
    class HolidayTrip
    {

        public Holiday holidayDestination { get; private set; }

        public DateTime startDate { get; private set; }
        public DateTime endDate { get; private set; }

        public decimal priceTotal { get; private set; }

        public HolidayTrip(Holiday destination, DateTime start, DateTime end)
        {
            holidayDestination = destination;
            startDate = start;
            endDate = end;

            TimeSpan travelTime = endDate - startDate;
            priceTotal = destination.PricePerDay * travelTime.Days;
        }

        public int durationInDays()
        {
            TimeSpan duration = endDate - startDate;
            return duration.Days;
        }

    }
}
