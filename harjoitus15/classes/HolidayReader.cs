﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace harjoitus15
{
    //luokka joka hallitsee lomapäivien lukemista xml tiedostosta.
    class HolidayReader
    {
        //lista lomamatkoista
        private static List<Holiday> holidayList = new List<Holiday>();

        //konstruktori lukee määritetyt lomamatkat data/availableHolidays.xml tiedostosta
        public HolidayReader()
        {
            
        }

        public void readXmlMemory()
        {
            //haetaan ohjelma sijainti
            string xmlFilePath = System.Windows.Forms.Application.StartupPath;

            //milloin "\\bin" osio alkaa 
            int startIndex = xmlFilePath.IndexOf("\\bin");

            //poistetaan bin osio ja sitä seuraavat kohdat
            xmlFilePath = xmlFilePath.Remove(startIndex, (xmlFilePath.Length - startIndex));

            //lisätään polku xml tiedostoon
            xmlFilePath = xmlFilePath + "\\data\\availableHolidays.xml";

            //ladataan xml tiedosto
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFilePath);

            //haetaan lomamatkat
            XmlNodeList items = doc.GetElementsByTagName("item");

            //mennään läpi jokainen löydetty lomamatka
            foreach (XmlNode node in items)
            {
                string name = "", description = "", destination = "";
                decimal price = 0;

                name = node.ChildNodes.Item(0).InnerText;
                description = node.ChildNodes.Item(1).InnerText;
                destination = node.ChildNodes.Item(3).InnerText;

                try
                {
                    price = Convert.ToDecimal(node.ChildNodes.Item(2).InnerText);
                }
                catch (Exception)
                {
                    //error
                }

                //lisätään lomamatka listaan
                Holiday holiday = new Holiday(name, destination, description, price);
                holidayList.Add(holiday);
            }
        }

        public List<Holiday> getHolidays() { return holidayList; }
        
    }
}
