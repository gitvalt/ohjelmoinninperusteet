﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace harjoitus15
{
    class userManager
    {
        //staattinen henkilöluokka "kirjautunut käyttäjä", joka tilaa matkoja
        public static User person { get; private set; }

        //hallitaan käyttäjiä
        public userManager()
        {
           
        }

        public void readXmlMemory()
        {
            //haetaan ohjelma sijainti
            string xmlFilePath = System.Windows.Forms.Application.StartupPath;

            //milloin "\\bin" osio alkaa 
            int startIndex = xmlFilePath.IndexOf("\\bin");

            //poistetaan bin osio ja sitä seuraavat kohdat
            xmlFilePath = xmlFilePath.Remove(startIndex, (xmlFilePath.Length - startIndex));

            //lisätään polku xml tiedostoon
            xmlFilePath = xmlFilePath + "\\data\\UserInfo.xml";

            //ladataan xml tiedosto
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFilePath);

            //haetaan lomamatkat
            XmlNode user = doc.LastChild;

            if (user == null)
            {
                person = null;
            }
            else
            {
                string first = user.ChildNodes.Item(0).InnerText;
                string last = user.ChildNodes.Item(1).InnerText;
                string phone = user.ChildNodes.Item(2).InnerText;
                string email = user.ChildNodes.Item(3).InnerText;
                string genre = user.ChildNodes.Item(4).InnerText;

                person = new User(first, last, email, phone);

                switch (genre.ToLower())
                {
                    case "man":
                        person.setGenre(User.genre.man);
                        break;
                    case "woman":
                        person.setGenre(User.genre.woman);
                        break;
                    case "other":
                        person.setGenre(User.genre.other);
                        break;
                }

                if (user.ChildNodes.Item(5).HasChildNodes == false)
                {
                    //Henkilöllä ei ole lomamatkoja määriteltynä   
                }
                else
                {
                    //henkilöllä on lomamatkoja määriteltynä
                    foreach (XmlNode item in user.ChildNodes.Item(5))
                    {
                        string destination = "";
                        DateTime start, end;
                        decimal total;

                        //luetaan jokainen tilattu lomamatka ja tallennetaan henkilön luokkaan
                        try
                        {
                            destination = item.ChildNodes.Item(0).InnerText;
                            start = Convert.ToDateTime(item.ChildNodes.Item(1).InnerText);
                            end = Convert.ToDateTime(item.ChildNodes.Item(2).InnerText);

                            HolidayReader reader = new HolidayReader();
                            Holiday origin = reader.getHolidays().First(holiday => holiday.Title == destination);

                            person.addHoliday(new HolidayTrip(origin, start, end));
                        }
                        catch (Exception)
                        {
                            //jokin meni pieleen
                        }
                    }
                }

            }
        }

        //määritä uusi henkilö | päivitä käyttäjän tiedot
        public void setNewUser(User new_person)
        {
            person = new_person;

            /*
            //haetaan ohjelma sijainti
            string xmlFilePath = System.Windows.Forms.Application.StartupPath;

            //milloin "\\bin" osio alkaa 
            int startIndex = xmlFilePath.IndexOf("\\bin");

            //poistetaan bin osio ja sitä seuraavat kohdat
            xmlFilePath = xmlFilePath.Remove(startIndex, (xmlFilePath.Length - startIndex));

            //lisätään polku xml tiedostoon
            xmlFilePath = xmlFilePath + "\\data\\UserInfo.xml";

            //ladataan xml tiedosto
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFilePath);

            XmlWriter write = XmlWriter.Create(xmlFilePath);

            write.WriteStartDocument();
 
            write.WriteStartElement("user");
            
            write.WriteElementString("firstname", person.Firstname);
            write.WriteEndDocument();
            */
        }

        public User getUser()
        {
            return person;
        }
    }   
}
