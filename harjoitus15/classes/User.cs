﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus15
{
    class User
    {
        //henkilökohtaiset tiedot
        public string Firstname { get; private set; }
        public string Lastname { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; private set; }

        public enum genre { man, woman, other };
        public genre userGenre { get; private set; }

        //lista tilatauista lomamatkoista
        public List<HolidayTrip> Orderedholidays = new List<HolidayTrip>();

        public User(string firstname, string lastname, string email, string phone, genre UserGenre)
        {
            Firstname = firstname;
            Lastname = lastname;
            Email = email;
            Phone = phone;
            userGenre = UserGenre;
        }

        public User(string firstname, string lastname, string email, string phone)
        {
            Firstname = firstname;
            Lastname = lastname;
            Email = email;
            Phone = phone;
        }

        public void addHoliday(HolidayTrip newInput)
        {
            Orderedholidays.Add(newInput);
        }

        

        public void setGenre(genre selection)
        {
            userGenre = selection;
        }
    }
}
