﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus15
{
    //lomapäivä luokka
    class Holiday
    {

        public string Title { get; private set; }
        public string Destination { get; private set; }
        public string Description { get; private set; }
        public decimal PricePerDay { get; private set; }

        public Holiday(string title, string destination, string description, decimal price)
        {
            Title = title;
            Description = description;
            Destination = destination;
            PricePerDay = price;
        }
        

    }
}
