﻿namespace harjoitus15
{
    partial class browseDestinations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.matkatDisplay = new System.Windows.Forms.DataGridView();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.matkakohde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.matkatDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 361);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(192, 44);
            this.button1.TabIndex = 2;
            this.button1.Text = "Takaisin alkuvalikkoon";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // matkatDisplay
            // 
            this.matkatDisplay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.matkatDisplay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Title,
            this.matkakohde,
            this.desc,
            this.price});
            this.matkatDisplay.Location = new System.Drawing.Point(12, 12);
            this.matkatDisplay.Name = "matkatDisplay";
            this.matkatDisplay.Size = new System.Drawing.Size(481, 264);
            this.matkatDisplay.TabIndex = 3;
            // 
            // Title
            // 
            this.Title.HeaderText = "Otsikko";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            // 
            // matkakohde
            // 
            this.matkakohde.HeaderText = "Matkakohde";
            this.matkakohde.Name = "matkakohde";
            this.matkakohde.ReadOnly = true;
            // 
            // desc
            // 
            this.desc.HeaderText = "Kuvaus";
            this.desc.Name = "desc";
            this.desc.ReadOnly = true;
            // 
            // price
            // 
            this.price.HeaderText = "Hinta per päivä";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            // 
            // browseDestinations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 430);
            this.Controls.Add(this.matkatDisplay);
            this.Controls.Add(this.button1);
            this.Name = "browseDestinations";
            this.Text = "Matkoja tarjolla";
            ((System.ComponentModel.ISupportInitialize)(this.matkatDisplay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView matkatDisplay;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn matkakohde;
        private System.Windows.Forms.DataGridViewTextBoxColumn desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
    }
}