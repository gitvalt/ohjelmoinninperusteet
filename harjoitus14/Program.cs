﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus14
{
    class Program
    {
        static void Main(string[] args)
        {
            //luodaan henkilo
            Person henkilo = new Person();

            //Maaritetaan syntymapaiva
            Console.WriteLine($"Määritetään käyttäjän '{henkilo.annaNimi()}' syntymäpäivä");
            henkilo.maaritaSyntymapaiva();

            //tulostetaan nykyinen tilanne
            Console.WriteLine(henkilo.ToString());

            henkilo.timeTillBirthDate();

            CreateEvent();

            Console.WriteLine("Ohjelma päättyy...");
            Console.ReadLine();
        }

        public static void CreateEvent()
        {
            Console.WriteLine("Luodaan tapahtuma:");

            bool jatka = true;
            string start = "", ends = "";
            Time_1 timeStart = new Time_1();
            Time_1 timeEnd = new Time_1();
            
            //Pyydetään tapahtuman alkamisaika käyttäjältä
            while (jatka) {

                Console.WriteLine("Tapahtuma alkaa:");
                start = Console.ReadLine();

                if(DateTime.TryParse(start, out DateTime res))
                {
                    try { 
                        timeStart.SetTime(res.Hour, res.Minute, res.Second);
                        jatka = false;
                    }
                    catch(ArgumentOutOfRangeException)
                    {
                        Console.WriteLine("Virhe tapahtuman alkamisen määrittämisessä");
                    }
                }
                else
                {
                    Console.WriteLine("Virhe! Ei ole aika");
                }
                
            }

            jatka = true;

            //Pyydetään tapahtuman loppumisaika käyttäjältä
            while (jatka)
            {

                Console.WriteLine("Tapahtuma päättyy:");
                ends = Console.ReadLine();

                if (DateTime.TryParse(ends, out DateTime res))
                {
                    try
                    {
                        timeEnd.SetTime(res.Hour, res.Minute, res.Second);
                        jatka = false;
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        Console.WriteLine("Virhe tapahtuman alkamisen määrittämisessä");
                    }
                }
                else
                {
                    Console.WriteLine("Virhe! Ei ole aika");
                }

            }

            Time_1 aikaaKaytetty = timeStart.timeSpent(timeEnd);

            Console.WriteLine($"Tapahtuma alkaa {timeStart.ToString()} ja päättyy {timeEnd.ToString()}. Tapahtuma kestää {aikaaKaytetty.getHour()} tuntia ja " +
                $"{aikaaKaytetty.getMinute()} minuuttia");

        }

    }
}
