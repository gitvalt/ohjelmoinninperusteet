﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus14
{
    class Date
    {
        private int month; // 1-12 
        private int day; // 1-31 based on month 
        public int Year { get; private set; } // auto-implemented property Year 

        // constructor: use property Month to confirm proper value for month; 
        // use property Day to confirm proper value for day 
        public Date(int month, int day, int year)
        {
            if(month < 1 || month > 12)
            {
                throw new FormatException($"Kuukausi '{month}' ei ole hyväksyttävä. Oltava 1 - 12 alueella");
            }
            else
            { 
                Month = month;
            }

            if(year < 1900 || year > 2100)
            {
                throw new FormatException($"Vuosi '{year}' ei ole hyväksyttävä. Oltava 1900 - 2100 alueella");
            }
            else
            {
                Year = year;
            }

            if(day < 1 || day > 31)
            {
                throw new FormatException($"Päivä '{day}' ei ole hyväksyttävä. Oltava 1 - 31 alueella");
            }
            else
            {
                Day = day; 
            }

            Console.WriteLine($"Päivä objekti luotu päivälle {this}");
        }

        public Date()
        {
            Console.WriteLine("Luodaan päivämäärää:");

            bool jatka = true;

            while (jatka) { 
                string date = Console.ReadLine();

                if (DateTime.TryParse(date, out DateTime res))
                {
                    Year = res.Year;
                    Month = res.Month;
                    Day = res.Day;
                    jatka = false;
                }
                else
                {
                    Console.WriteLine("Ei hyväksyttävä päivämäärä");
                }
            }
            
        }

        // property that gets and sets the month 
        public int Month
        {
            get
            {
                return month;
            }
            private set // make writing inaccessible outside the class 
            {
                if (value <= 0 || value > 12) // validate month 
                {
                    throw new ArgumentOutOfRangeException(
                    nameof(value), value, $"{nameof(Month)} must be 1-12");
                }

                month = value;
            }
        }

        // property that gets and sets the day 
        public int Day
        {
            get
            {
                return day;
            }
            private set // make writing inaccessible outside the class 
            {
                int[] daysPerMonth =
                {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

                // check if day in range for month 
                if (value <= 0 || value > daysPerMonth[Month])
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value,
                    $"{nameof(Day)} out of range for current month/year");
                }
                // check for leap year 
                if (Month == 2 && value == 29 &&
                !(Year % 400 == 0 || (Year % 4 == 0 && Year % 100 != 0)))
                {
                    throw new ArgumentOutOfRangeException(nameof(value), value,
                    $"{nameof(Day)} out of range for current month/year");
                }

                day = value;
            }
        }

        public string Weekday(DateTime date)
        {
            string weekday = date.DayOfWeek.ToString();

            string localization = "";

            switch (weekday)
            {
                case "Monday":
                    localization = "Maanantai";
                    break;
                case "Tuesday":
                    localization = "Tiistai";
                    break;
                case "Wensday":
                    localization = "Keskiviikko";
                    break;
                case "Thursday":
                    localization = "Torstai";
                    break;
                case "Friday":
                    localization = "Perjantai";
                    break;
                case "Saturday":
                    localization = "Lauantai";
                    break;
                case "Sunday":
                    localization = "Sunnuntai";
                    break;
            }

            
            return localization;
        }

        public string Weekday()
        {
            string weekday = new DateTime(Year, Month, Day).DayOfWeek.ToString();

            string localization = "";

            switch (weekday)
            {
                case "Monday":
                    localization = "Maanantai";
                    break;
                case "Tuesday":
                    localization = "Tiistai";
                    break;
                case "Wensday":
                    localization = "Keskiviikko";
                    break;
                case "Thursday":
                    localization = "Torstai";
                    break;
                case "Friday":
                    localization = "Perjantai";
                    break;
                case "Saturday":
                    localization = "Lauantai";
                    break;
                case "Sunday":
                    localization = "Sunnuntai";
                    break;
            }


            return localization;
        }

        public void daysTillBirthDate()
        {
            //get birthday day and month
            DateTime temp = new DateTime(DateTime.Now.Year, Month, Day);

            //time difference between now and date
            TimeSpan paiviaJaljella =  temp - DateTime.Now;

            int paiviaJ = Math.Abs(paiviaJaljella.Days);

            int seuraava_vuosi = 0;

            //if birth date has already passed
            if(paiviaJaljella.Days < 0)
            {
                DateTime new_temp = temp.AddYears(1);

                paiviaJaljella = new_temp - DateTime.Now;
                seuraava_vuosi = Math.Abs(paiviaJaljella.Days);
            }
            
            switch (DateTime.Compare(temp, DateTime.Now))
            {
                //syntymapaiva on jo tapahtunut
                case -1:
                    Console.WriteLine($"Syntymäpäivä on jo tapahtunut {paiviaJ} päivää sitten.");
                    Console.WriteLine($"Seuraava syntymäpäivä tapahtuu {seuraava_vuosi}.");
                    break;
                //syntymapaiva tapahtuu tänään
                case 0:
                    Console.WriteLine($"Syntymäpäivä on tänään.");
                    break;
                //syntymapaiva ei ole vielä tapahtunut
                case 1:
                    Console.WriteLine($"Syntymäpäivään on vielä {paiviaJ} päivää jäljellä.");
                    break;
            }

            Console.WriteLine($"Seuraava syntymäpäivä on viikonpäivänä: " + Weekday(temp));

        }

        // return a string of the form month/day/year 
        public override string ToString() => $"{Month}/{Day}/{Year}";

        /************************************************************************** 
         * (C) Copyright 1992-2017 by Deitel & Associates, Inc. and * 
         * Pearson Education, Inc. All Rights Reserved. * 
         * * 
         * DISCLAIMER: The authors and publisher of this book have used their * 
         * best efforts in preparing the book. These efforts include the * 
         * development, research, and testing of the theories and programs * 
         * to determine their effectiveness. The authors and publisher make * 
         * no warranty of any kind, expressed or implied, with regard to these * 
         * programs or to the documentation contained in these books. The authors * 
         * and publisher shall not be liable in any event for incidental or * 
         * consequential damages in connection with, or arising out of, the * 
         * furnishing, performance, or use of these programs. * 
         *************************************************************************/
    }

}
