﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus14
{
    class Person
    {
        private string Etunimi;
        private string Sukunimi;

        private string Ammatti;

        private Date Syntymapaiva;
        
        public Person(string etunimi, string sukunimi, string ammatti, Date syntymapaiva)
        {
            Etunimi = etunimi;
            Sukunimi = sukunimi;
            Ammatti = ammatti;
            Syntymapaiva = syntymapaiva;
        }
        
        public Person()
        {
            Console.WriteLine("Luodaan henkilön profiilia...");

            bool jatka = true;
            string firstname = "", lastname = "", career = "";

            while (jatka) { 
                Console.WriteLine("Anna etunimi:");
                firstname = Console.ReadLine();
                
                if(firstname == "")
                {
                    Console.WriteLine("Etunimi ei voi olla tyhjä");
                }
                else
                {
                    jatka = false;
                }
            }

            jatka = true;

            while (jatka)
            {
                Console.WriteLine("Anna sukunimi:");
                lastname = Console.ReadLine();

                if (lastname == "")
                {
                    Console.WriteLine("Sukunimi ei voi olla tyhjä");
                }
                else
                {
                    jatka = false;
                }
            }

            jatka = true;
            
            while (jatka)
            {
                Console.WriteLine("Anna ammatti:");
                career = Console.ReadLine();

                if (career == "")
                {
                    Console.WriteLine("Ammatti ei voi olla tyhjä");
                }
                else
                {
                    jatka = false;
                }
            }

            Etunimi = firstname;
            Sukunimi = lastname;
            Ammatti = career;

            Console.WriteLine($"Henkilö {annaNimi()} luotu. Ammatti {career}");

        }

        public string annaEtunimi() { return Etunimi; }
        public string annaSukunimi() { return Etunimi; }
        public string annaAmmatti() { return Etunimi; }
        public Date annaSyntymapaiva() { return Syntymapaiva; }

        public void maaritaSyntymapaiva()
        {
            Syntymapaiva = new Date();
        }

        public string annaNimi() { return Etunimi + " " + Sukunimi; }

        public void timeTillBirthDate()
        {
            Syntymapaiva.daysTillBirthDate();
        } 

        public override string ToString()
        {
            return ($"Henkilö: {Etunimi} {Sukunimi}, {Ammatti}, {Syntymapaiva.ToString()}");
        }
    }
}
