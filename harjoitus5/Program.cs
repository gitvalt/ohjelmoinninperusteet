﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus5
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, User> kayttajat = new Dictionary<string, User>();

            bool jatka = true;
            string Username = "", Password = "";

            while (jatka) {
                
                Console.WriteLine("Anna käyttäjätunnus");
                Username = Console.ReadLine();

                if(Username.Length <= 5)
                {
                    Console.WriteLine("Virhe! Käyttäjätunnus täytyy olla pituudeltaan suurempi tai yhtäsuuri kuin 5");
                } else if (kayttajat.ContainsKey(Username))
                {
                    Console.WriteLine("Virhe! Käyttäjätunnus on jo olemassa");
                } else
                {
                    jatka = false;
                }
            }

            jatka = true;

            while (jatka)
            {
                Console.WriteLine("Anna salasana");
                Password = Console.ReadLine();

                if(Password.Length < 8 || Password.Length > 20)
                {
                    Console.WriteLine("Virhe! Salasanan täytyy olla 8 ja 20 merkin pituuden väliltä");
                } else
                {
                    jatka = false;
                    User kayttaja = new User(Username, Password);
                    Console.WriteLine($"Käyttäjä {Username} luotu");
                    kayttajat.Add(Username, kayttaja);
                }

            }

            Console.ReadLine();
        }
    }
}
