﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus5
{
    class User
    {
        private string Kayttajatunnus;
        private string Salasana;

        public User(string kayttajatunnus, string salasana)
        {
            bool luo_kauttaja = maaritaKayttaja(kayttajatunnus);
            bool luo_salasana = maaritaSalasana(salasana);
        }

        private bool maaritaKayttaja(string kayttajatunnus)
        {

            Kayttajatunnus = kayttajatunnus;
            return true;
        }

        private bool maaritaSalasana(string salasana)
        {
            Salasana = salasana;
            return true;
        }

        public string annaKayttajatunnus()
        {
            return Kayttajatunnus;
        }

        public string annaSalasana()
        {
            return Salasana;
        }

        public override string ToString()
        {
            return($"Käyttäjä {Kayttajatunnus} on olemassa");
        }

    }
}
