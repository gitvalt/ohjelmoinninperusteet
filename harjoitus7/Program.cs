﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus7
{
    class Program
    {
        static void Main(string[] args)
        {
            Craps peli = new Craps();
        }
    }
    
    // Fig. 7.7: Craps.cs 
    // Craps luokka simuloi noppapeliä "craps"

    //Määritetään mitä kirjastoja käytetään
    //using System; 


    class Craps
    {

        public Craps()
        {
            Loop();
        }

        //Luodaan Random objekti, jonka avulla voidaan hakea satunnaisesti keksittyjä lukuja
        private static Random randomNumbers = new Random();

        //Luodaan enum-lista, jossa määritetään pelin mahdolliset tilat (jatka, peli voitettu, peli hävitty)
        private enum Status { Continue, Won, Lost }

        //Luodaan toinen lista, joka sisältää kaikista yleisimmät nopan tulokset niminä. Esim. jos saataisiin tulokseksi "SnakeEyes" niin hakemalla sitä tästä listasta antaisi tulokseksi arvon 2.

        private enum DiceNames
        {
            SnakeEyes = 2,
            Trey = 3,
            Seven = 7,
            YoLeven = 11,
            BoxCars = 12
        }

        //Luodaan ohjelman päälooppi
        public static void Loop()
        {

            //Määritetään pelin alkustatus tilaan "jatka" ja pistemäärä nollataan
            Status gameStatus = Status.Continue;
            int myPoint = 0;

            //Heitetään noppia
            int sumOfDice = RollDice();

            //Tutkitaan mikä tulos saatiin nopista
            switch ((DiceNames)sumOfDice)
            {
                //Jos ensimmäisellä heittämällä saatiin 7 tai 11 niin peli voitettiin
                case DiceNames.Seven:
                case DiceNames.YoLeven:
                    gameStatus = Status.Won;
                    break;

                //Jos ensimmäisellä heittämällä saataisiin nopan arvoksi 2, 3, 12 niin peli hävitään.
                case DiceNames.SnakeEyes:
                case DiceNames.Trey:
                case DiceNames.BoxCars:
                    gameStatus = Status.Lost;
                    break;

                //Jos saatu arvo ei ole 2, 3, 12, 7, 11 niin peli jatkuu toiselle kierrokselle
                default:
                    gameStatus = Status.Continue; // peliä jatketaan
                    myPoint = sumOfDice; // tallennettaan saatu tulos pisteisiin 
                    Console.WriteLine($"Point is {myPoint}"); //kirjoitetaan konsoliin saatu tulos
                    break;
            }

            //Jos peli jatkuu seuraavalle kierrokselle. Noppaa heitetään uudestaan ja uudestaan kunnes saadaan tulokseksi luku 7 tai sama mitä saatiin ensimmäisellä kierroksella.
            while (gameStatus == Status.Continue)
            {
                sumOfDice = RollDice(); // heitetään noppia uudelleen 

                //jos saatu uusi tulos on sama kuin edellisellä kierroksella saatu luku niin voitetaan
                if (sumOfDice == myPoint)
                {
                    gameStatus = Status.Won;
                }
                else
                {
                    // Jos saatu tulos on seitsemän, peli hävitään 
                    if (sumOfDice == (int)DiceNames.Seven)
                    {
                        gameStatus = Status.Lost;
                    }
                }
            }

            // Kun peli ollaan joko voitettu tai hävitty
            if (gameStatus == Status.Won)
            {
                //peli ollaan voitettu
                Console.WriteLine("Player wins");
            }
            else
            {
                //peli ollaan hävitty
                Console.WriteLine("Player loses");
            }
            //ohjelma päättyy kun käyttäjä painaa jotain näppäintä.
            Console.ReadLine();
        }

        // nopan heittoon käytetty metodi 
        static int RollDice()
        {
            //heitetään noppaa 2 kertaa hakemalla satunnaislukugeneraattorilta kaksi lukua 1 ja 7 väliltä.
            int die1 = randomNumbers.Next(1, 7);
            int die2 = randomNumbers.Next(1, 7);

            int sum = die1 + die2; // summataan saadu sattunnaisluvut 

            //kirjoitetaan saatu summa konsoliin
            Console.WriteLine($"Player rolled {die1} + {die2} = {sum}");
            return sum; // palautetaan saatu summa main-loopille
        }
    }
}
    

//lisenssointi ehtoja
/************************************************************************** 
 * (C) Copyright 1992-2017 by Deitel & Associates, Inc. and * 
 * Pearson Education, Inc. All Rights Reserved. * 
 * * 
 * DISCLAIMER: The authors and publisher of this book have used their * 
 * best efforts in preparing the book. These efforts include the * 
 * development, research, and testing of the theories and programs * 
 * to determine their effectiveness. The authors and publisher make * 
 * no warranty of any kind, expressed or implied, with regard to these * 
 * programs or to the documentation contained in these books. The authors * 
 * and publisher shall not be liable in any event for incidental or * 
 * consequential damages in connection with, or arising out of, the * 
 * furnishing, performance, or use of these programs. * 
}
*/