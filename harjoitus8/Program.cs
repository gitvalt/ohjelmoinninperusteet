﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace harjoitus8
{
    class Program
    {
        //Kolme staattista julkista muuttujaa
        public static float kanta;
        public static float korkeus;
        public static float pinta_ala;

        static void Main(string[] args)
        {
            //pyydetään käyttäjältä kolmion tiedot
            Console.WriteLine("Kolmion pinta-alan laskemisohjelma");
            haeArvot();

            //lasketaan pinta-ala
            pinta_ala = laskeKolmionPinta_ala(kanta, korkeus);

            //tulostetaan saadut tulokset
            Console.WriteLine($"Kolmion kanta: {kanta}m, korkeus: {korkeus}m, pinta-ala: {pinta_ala}m^2");

            //päätetään ohjelma
            Console.WriteLine("Ohjelma päättyy...");
            Console.ReadLine();

        }

        //Luetaan ja sijoitetaan kanta ja korkeus käyttäjän syötteistä
        static void haeArvot()
        {
            //paikallinen syote, tulos, jatka muuttujat
            string syote = "";
            float tulos = 0;
            bool jatka = true;

            //pyydetään kolmion kannan pituutta kunnes annettu syöte on hyväksyttävä
             while (jatka) {
                Console.WriteLine("Anna kolmion kannan pituus (m)");
                syote = Console.ReadLine();
                if(float.TryParse(syote, out float result))
                {
                    tulos = Convert.ToInt64(syote);
                    jatka = false;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Syote ei ole hyväksyttävä luku");
                    Console.ResetColor();
                }
            }
            
            //tallennetaan tulos ja resetoidaan paikalliset muuttujat
            jatka = true;
            kanta = tulos;
            tulos = 0;

            //pyydetään syötteenä kolmion korkeutta, kunnes on annettu hyväksyttävä arvo
            while (jatka)
            {
                Console.WriteLine("Anna kolmion korkeus (m)");
                syote = Console.ReadLine();
                if (float.TryParse(syote, out float result))
                {
                    tulos = Convert.ToInt64(syote);
                    jatka = false;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Syote ei ole hyväksyttävä luku");
                    Console.ResetColor();
                }
            }

            korkeus = tulos;
        }

        //lasketaan ja sijoitetaan pinta-ala
        static float laskeKolmionPinta_ala(float kanta, float korkeus)
        {
            return ((kanta * korkeus) / 2);
        }

        //sama metodi, mutta floatin sijasta int --> laskeKolmionPinta_ala kuormitus
        static int laskeKolmionPinta_ala(int kanta, int korkeus)
        {
            return ((kanta * korkeus) / 2);
        }
    }
}
